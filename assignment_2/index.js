
const FIRST_NAME = "Andreea";
const LAST_NAME = "Popovici";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
    
    
    var VacantObject={} ;

    var myObject={} ;

    myObject.pageAccessCounter=function (accessedWebs) {
        
        if (accessedWebs === undefined)
            {
                if(VacantObject['home']===undefined)
            VacantObject['home'] =1;
            else
            VacantObject['home'] +=1;
            }
           
        else 
        {   
            accessedWebs=accessedWebs.toLowerCase();
            if(VacantObject[accessedWebs]===undefined)
            VacantObject[accessedWebs] =1;
            else
            VacantObject[accessedWebs] =VacantObject[accessedWebs]+1;
            
        }
    }

    myObject.getCache= function ()
    {
    return VacantObject;
    }
    
    return myObject;

 
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

